package algo;

import java.util.Comparator;

public interface Sorter<T extends Comparable<T> > {
    List<T> sort(List<T> list);
    List<T> sort(List<T> list, Comparator<T> comp);
}